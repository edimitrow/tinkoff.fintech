//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {

    @IBOutlet weak var conversationTableView: UITableView!
    
    var messages: [Message]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messages = conversationStorage.chats.joined().first(where:{$0.isActive! == true})?.messages
        self.title = conversationStorage.chats.joined().first(where:{$0.isActive! == true})?.name
        
        conversationTableView.dataSource = self
        
        conversationTableView.estimatedRowHeight = 100.0
        conversationTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        conversationStorage.chats.first?.first(where:{$0.isActive! == true})?.isActive = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}

extension ConversationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = messages {
            return messages!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message: Message = messages![indexPath.row]
        let identifier = message.isIncoming == true ? "ConversationInCell" : "ConversationOutCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ConversationCell

        if message.isIncoming == true {
            cell.incomingLeadingConstraint.constant = 16.0
            cell.incomingTrailingConstraint.constant = cell.bounds.width / 4
        } else {
            cell.outgoingLeadingConstraint.constant = cell.bounds.width / 4
            cell.outgoingTrailingConstraint.constant = 16.0
        }
        
        cell.messageLabel.text = message.text
        
        return cell
    }
}

//
//  ConversationCell.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet weak var outgoingLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var outgoingTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var incomingLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var incomingTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var messageBackgroundView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

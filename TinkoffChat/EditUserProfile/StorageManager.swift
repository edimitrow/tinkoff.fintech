//
//  StorageManager.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/30/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import Foundation


class GCDSavingDataManager: NSObject {
    
    func saveDataToFile(_ jsonData: Data, completion: @escaping () -> Void, failure: @escaping () -> Void) {
        
        let fileURL = URL(fileURLWithPath: ProfileConstanses.filePath)
        let queue = DispatchQueue.global(qos: .default)
        queue.async {
            do {
                try jsonData.write(to: fileURL, options: [])
                DispatchQueue.main.async {
                    completion()
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    failure()
                }
            }
        }
    }
}

class OperationSavingDataManager: NSObject {
    
    func saveDataToFile(_ jsonData: Data, completion: @escaping () -> Void, failure: @escaping () -> Void) {
        
        let fileURL = URL(fileURLWithPath: ProfileConstanses.filePath)
        let queue = OperationQueue()
        queue.name = "ProfileSavingQueue"
        queue.maxConcurrentOperationCount = 3
        queue.addOperation {
            do {
                try jsonData.write(to: fileURL, options: [])
                OperationQueue.main.addOperation({
                    completion()
                })
            } catch {
                print(error)
                OperationQueue.main.addOperation({
                    failure()
                })
            }
        }
    }
}


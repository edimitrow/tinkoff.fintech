//
//  EditUserProfileViewController.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 2/21/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

enum SaveType: Int {
    case gcd = 0
    case operation = 1
}

class EditUserProfileViewController: UIViewController {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var choosePhotoButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var userDescriptionLabel: UILabel!
    
    @IBOutlet weak var gcdButton: UIButton!
    @IBOutlet weak var operationQueueButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    var userProfileModel: UserProfileModel?
    var baseUserProfileModel: UserProfileModel?
    
    //MARK: - view controller lifecycle:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        baseUserProfileModel = UserProfileManager().fillUserProfileModelWithData()
        userProfileModel = UserProfileManager().fillUserProfileModelWithData()
        showPersonalInfo(userProfileModel!)
        toggleVisibility(false)
        enableEditing(false)
        activityIndicator.isHidden = true
        nameTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(EditUserProfileViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditUserProfileViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        roundViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func showPersonalInfo(_ userProfileModel: UserProfileModel) {
        
        nameTextField.text = userProfileModel.userName
        userDescriptionLabel.text = userProfileModel.userProfileInfo
        userPhotoImageView.image = userProfileModel.userProfilePicture
    }
    
    //MARK: - handle keyboard:
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardFrame = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight: CGFloat
            if #available(iOS 11.0, *) {
                keyboardHeight = keyboardFrame.height - self.view.safeAreaInsets.bottom
            } else {
                keyboardHeight = keyboardFrame.height
            }
            self.view.frame.origin.y = UIApplication.shared.statusBarFrame.height + (self.navigationController?.navigationBar.frame.height)! - keyboardHeight
            
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                    self.operationQueueButton.alpha = 0.0
                    self.gcdButton.alpha = 0.0
                })
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = UIApplication.shared.statusBarFrame.height + (self.navigationController?.navigationBar.frame.height)!
            
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                    self.mapModel()
                    self.showPersonalInfo(self.userProfileModel!)
                    self.enableToSave(UserProfileManager().checkForChanges(self.baseUserProfileModel!, newModel: self.userProfileModel!))

                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - setup ui:
    
    func roundViews() {
        
        let viewRadius = choosePhotoButton.frame.width / 2.0
        choosePhotoButton.layer.cornerRadius = viewRadius
        choosePhotoButton.layer.masksToBounds = true
        choosePhotoButton.imageView?.contentMode = .scaleAspectFit
        choosePhotoButton.imageEdgeInsets = UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)
        
        userPhotoImageView.layer.cornerRadius = viewRadius
        userPhotoImageView.layer.masksToBounds = true
        
        makeButtonRounded(gcdButton, radius: viewRadius)
        makeButtonRounded(operationQueueButton, radius: viewRadius)

    }
    
    func makeButtonRounded(_ button: UIButton, radius: CGFloat) {
        
        button.layer.cornerRadius = radius / 3.0
        button.layer.backgroundColor = UIColor.white.cgColor
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.black.cgColor
        
    }
    
    func toggleVisibility(_ enable: Bool) {
        
        choosePhotoButton.isHidden = !enable
        gcdButton.isHidden = !enable
        operationQueueButton.isHidden = !enable
        descriptionTextField.isHidden = !enable
        descriptionTextField.text = userProfileModel?.userProfileInfo
        
    }
    
    func enableToSave(_ enable: Bool) {

        gcdButton.isUserInteractionEnabled = enable
        operationQueueButton.isUserInteractionEnabled = enable
        DispatchQueue.main.async { [unowned self] in
            
            if enable {
                self.gcdButton.backgroundColor = UIColor.white
                self.gcdButton.alpha = 1.0
                self.operationQueueButton.backgroundColor = UIColor.white
                self.operationQueueButton.alpha = 1.0
            } else {
                self.gcdButton.backgroundColor = UIColor.lightGray
                self.gcdButton.alpha = 0.4
                self.operationQueueButton.backgroundColor = UIColor.lightGray
                self.operationQueueButton.alpha = 0.4
            }
        }
    }
    
    func enableEditing(_ enable: Bool) {
        
        choosePhotoButton.isUserInteractionEnabled = enable
        nameTextField.isUserInteractionEnabled = enable
        userDescriptionLabel.isUserInteractionEnabled = enable
        userDescriptionLabel.isHidden = enable
        enableToSave(false)
    }
    
    func mapModel() {

        userProfileModel?.userName = nameTextField.text
        userDescriptionLabel.text = descriptionTextField.text
        userProfileModel?.userProfileInfo = userDescriptionLabel.text
        userProfileModel?.userProfilePicture = userPhotoImageView.image

    }
    
    //MARK: - actions
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        saveAttempt(SaveType(rawValue: sender.tag)!)
    }
    
    func saveAttempt(_ type: SaveType) {
        
        toggleVisibility(true)
        
        mapModel()
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        self.activityIndicator.hidesWhenStopped = true
        
        let data = UserProfileManager().mapModelToData(userProfileModel!)
        enableToSave(false)
        switch type {
        case SaveType.gcd:
            GCDSavingDataManager().saveDataToFile(data!, completion: { [unowned self] in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
                
                self.enableEditing(false)
                self.toggleVisibility(false)
                }, failure: { [unowned self] in
                    self.showErrorAlert()
            })
            break
        case SaveType.operation:
            OperationSavingDataManager().saveDataToFile(data!, completion: { [unowned self] in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
                
                self.enableEditing(false)
                self.toggleVisibility(false)
                }, failure: { [unowned self] in
                    self.showErrorAlert()
            })
            break
        }
    }
    
    func showErrorAlert() {
        
        let alertController = UIAlertController(title: "Error", message: "Something goes wrong this time, wanna try again?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { [unowned self] _ in
            self.enableEditing(false)
            self.toggleVisibility(false)
        }
        
        let gcdAction = UIAlertAction(title: "try GCD", style: UIAlertActionStyle.default) { [unowned self] _ in
            
            self.saveAttempt(.gcd)
        }
        
        let operationAction = UIAlertAction(title: "try GCD", style: UIAlertActionStyle.default) { [unowned self] _ in
            
            self.saveAttempt(.operation)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(gcdAction)
        alertController.addAction(operationAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        enableEditing(true)
        toggleVisibility(true)
    }
    
    @IBAction func choosePhotoAction(_ sender: UIButton) {
        
        print("\n *** CHOOSE PROFILE IMAGE ACTION *** ")
        
        let profilePhotoPicker = UIImagePickerController()
        profilePhotoPicker.delegate = self
        profilePhotoPicker.allowsEditing = false
        
        let photoSourceActionSheet = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction.init(title: "Camera", style: .default) { _ in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                profilePhotoPicker.sourceType = .camera
                if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.front) {
                    profilePhotoPicker.cameraDevice = .front
                }
                
                profilePhotoPicker.cameraCaptureMode = .photo
                profilePhotoPicker.modalPresentationStyle = .fullScreen
                self.present(profilePhotoPicker,animated: true, completion: nil)
            } else {
                // handle case when there's no camera somehow
            }
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in
            profilePhotoPicker.sourceType = .photoLibrary
            profilePhotoPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(profilePhotoPicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        photoSourceActionSheet.addAction(cameraAction)
        photoSourceActionSheet.addAction(galleryAction)
        photoSourceActionSheet.addAction(cancelAction)
        
        self.present(photoSourceActionSheet, animated: true, completion: nil)
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

extension EditUserProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

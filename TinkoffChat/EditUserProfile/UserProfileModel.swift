//
//  UserProfileModel.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/30/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class UserProfileModel {

    var userProfilePicture: UIImage?
    var userProfileInfo: String?
    var userName: String?
    
}

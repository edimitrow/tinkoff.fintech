//
//  EditUserProfileViewControllerExtension.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/6/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

extension EditUserProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userPhotoImageView.contentMode = .scaleAspectFill
            userPhotoImageView.image = chosenImage
            userProfileModel?.userProfilePicture = chosenImage
            
        }
        
        dismiss(animated:true) { [unowned self] in
            self.enableToSave(UserProfileManager().checkForChanges(self.baseUserProfileModel!, newModel: self.userProfileModel!))
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

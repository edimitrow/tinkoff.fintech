//
//  UserProfileManager.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/30/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class ProfileConstanses {
    
    static let filePath: String = NSHomeDirectory() + "/Documents/" + "userInfo"
    static let nameKey = "name"
    static let infoKey = "info"
    static let imageKey = "image"
}



class UserProfileManager: NSObject {
    
    func fillUserProfileModelWithData() -> UserProfileModel {
        
        let userProfileModel = UserProfileModel()
        
        let fileURL = URL(fileURLWithPath: ProfileConstanses.filePath)
        do {
            if let data = try? Data(contentsOf: fileURL, options: []) {
                
                let parsedData = try JSONSerialization.jsonObject(with: data) as? [String: String]
                userProfileModel.userName = parsedData?[ProfileConstanses.nameKey]
                userProfileModel.userProfileInfo = parsedData?[ProfileConstanses.infoKey]
                let userImageDataString = parsedData![ProfileConstanses.imageKey]
                let imageData = Data(base64Encoded: userImageDataString!, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
                userProfileModel.userProfilePicture = UIImage(data: imageData!)
                
            } else {
                
                userProfileModel.userName = "Gene Dimitrow"
                userProfileModel.userProfileInfo = "Guess here comes some personal info of mine"
                userProfileModel.userProfilePicture = UIImage(named: "emptyUser")
            }
 
        } catch {
            print(error)
        }
        return userProfileModel
    }
    
    
    func mapModelToData(_ userProfileModel: UserProfileModel) -> Data? {
        
        let imageStr64 = getStringFromImage(userProfileModel.userProfilePicture!)
        let savingDictionary = [ProfileConstanses.nameKey: userProfileModel.userName, ProfileConstanses.infoKey: userProfileModel.userProfileInfo, ProfileConstanses.imageKey: imageStr64]

        var jsonData: Data?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: savingDictionary, options: [])
        } catch {
            print("Error creating json \(error)")
        }
        return jsonData

    }
    
    func checkForChanges(_ oldModel: UserProfileModel, newModel: UserProfileModel) -> Bool {
        
        if (oldModel.userName == newModel.userName) && (oldModel.userProfileInfo == newModel.userProfileInfo) && (getStringFromImage(oldModel.userProfilePicture!) == getStringFromImage(newModel.userProfilePicture!)){
            return false
        }
        return true

    }
    
    private func getStringFromImage(_ image: UIImage) -> String {

        let imageData: Data =  UIImageJPEGRepresentation(image, 0.5)!
        let imageStr64 = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        return imageStr64
    }
}


//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 2/21/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

let conversationStorage = ConversationStorage()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        if let storedTheme = ThemesManager.retriveStoredTheme() {
            ThemesManager.setThemeWithMainColor(storedTheme)
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {

    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {

    }
    
    func applicationWillTerminate(_ application: UIApplication) {

    }
}

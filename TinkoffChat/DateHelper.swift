//
//  DateHelper.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import Foundation

enum DateFormat {
    
    case hoursMinutes, dayOfMonth
}

class DateHelper {

    static func getRandomDate() -> Date {
        
        let date = Date()
        var offsetComponents = DateComponents()
        offsetComponents.day = Int(arc4random_uniform(4)) * (-1)
        offsetComponents.minute = Int(arc4random_uniform(59))
        offsetComponents.hour = Int(arc4random_uniform(23))
        let calendar = Calendar.current
        
        let dateWithOffset = calendar.date(byAdding: offsetComponents, to: date)
        
        return dateWithOffset!
    }
    
//    static func getDateBackFor(_ days: Int, date: Date) -> Date {
//
//        var offsetComponents = DateComponents()
//        offsetComponents.day = days
//        let calendar = Calendar.current
//        let dateWithOffset = calendar.date(byAdding: offsetComponents, to: date)
//        return dateWithOffset!
//    }
//
//    static func getSameDateBackFor(_ minutes: Int, date: Date) -> Date {
//
//        var offsetComponents = DateComponents()
//        offsetComponents.minute = minutes
//        let calendar = Calendar.current
//        let dateWithOffset = calendar.date(byAdding: offsetComponents, to: date)
//        return dateWithOffset!
//    }
    
    static func checkIfDateTheSame(dateOne:Date, dateTwo:Date) -> Bool {
        
        let calendar = Calendar.current
        let calendarUnitFlags = Set<Calendar.Component>([.day, .month, .year, .hour])
        let compOne = calendar.dateComponents(calendarUnitFlags, from: dateOne)
        let compTwo = calendar.dateComponents(calendarUnitFlags, from: dateTwo)
        let check:Bool = (compOne.day == compTwo.day && compOne.month == compTwo.month && compOne.year == compTwo.year) ? true : false
        return check
    }
    
    static func formatDate(_ date: Date, format: DateFormat) -> String {
        
        let dateFormatter = DateFormatter()
        
        switch format {
        case .hoursMinutes:
            dateFormatter.dateFormat = "HH:mm"
            break
        case .dayOfMonth:
            dateFormatter.dateFormat = "dd MMM"
            break
        }
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}

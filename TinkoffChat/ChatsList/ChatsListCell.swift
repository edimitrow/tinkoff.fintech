//
//  ChatsListCell.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/11/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class ChatsListCell: UITableViewCell, ConversationCellConfiguration {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

protocol ConversationCellConfiguration: class {
    
    var name: String? {get set}
    var message: String? {get set}
    var date: Date? {get set}
    var onLine: Bool? {get set}
    var hasUnreadMessages: Bool? {get set}
    
}

extension ConversationCellConfiguration {
    
    var name: String? {
        get { return name}
        set { name = newValue }
    }
    var message: String? {
        get { return message}
        set { message = newValue }
    }
    var date: Date? {
        get { return date}
        set { date = newValue }
    }
    var onLine: Bool? {
        get { return onLine}
        set { onLine = newValue }
    }
    var hasUnreadMessages: Bool? {
        get { return hasUnreadMessages}
        set { hasUnreadMessages = newValue }
    }
    
    func configureCell<C: ChatsListCell>(_ cell: C, at index: IndexPath) {
        
        cell.nameLabel.text = conversationStorage.chats[index.section][index.row].name
        
        if let message = conversationStorage.chats[index.section][index.row].messages?.last {
            if let text = message.text {
                if message.isRead! == false {
                    cell.messageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                } else {
                    cell.messageLabel.font = UIFont.systemFont(ofSize: 15.0)
                }
                cell.messageLabel.text = text
            }
        } else {
            cell.messageLabel.font = UIFont.italicSystemFont(ofSize: 17.0)
            cell.messageLabel.text = "NO MESSAGES YET"
        }

        if conversationStorage.chats[index.section][index.row].isOnline == true {
            cell.contentView.backgroundColor = UIColor(red: 0.878, green: 0.859, blue: 0.792, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor.white
        }
        
        if let messageDate = conversationStorage.chats[index.section][index.row].messages?.last?.date {
            if DateHelper.checkIfDateTheSame(dateOne: messageDate, dateTwo: Date()) {
                cell.dateLabel.text = DateHelper.formatDate(messageDate, format: .hoursMinutes)
            } else {
                cell.dateLabel.text = DateHelper.formatDate(messageDate, format: .dayOfMonth)
            }
        } else {
            cell.dateLabel.text = ""
        }
    }
    
}

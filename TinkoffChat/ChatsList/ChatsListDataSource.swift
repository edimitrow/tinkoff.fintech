//
//  ChatsListDataSource.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/11/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

extension ChatsListViewController: UITableViewDataSource {
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return conversationStorage.chats.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversationStorage.chats[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsListCell", for: indexPath) as? ChatsListCell
        cell?.configureCell(cell!, at: indexPath)
        return cell!
    }
}

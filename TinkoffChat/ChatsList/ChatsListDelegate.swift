//
//  ChatsListDelegate.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/11/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

extension ChatsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        conversationStorage.chats[indexPath.section][indexPath.row].isActive = true
        self.performSegue(withIdentifier: "ConversationSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionTitle: String = ""
        let count = conversationStorage.chats.joined().count
        switch section {
        case 0:
            sectionTitle = " Online - \(conversationStorage.chats[section].count)/\(count)"
            break
        case 1:
            sectionTitle = " Offline - \(conversationStorage.chats[section].count)/\(count)"
            break
        default:
            break
        }
        return sectionTitle
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
}

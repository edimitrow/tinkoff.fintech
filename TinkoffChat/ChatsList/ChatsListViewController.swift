//
//  ChatsListViewController.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/11/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class ChatsListViewController: UIViewController {

    @IBOutlet weak var chatsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatsTableView.delegate = self
        chatsTableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func openProfileAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "ProfileSegue", sender: self)
    }
    
    @IBAction func openSettingsAction(_ sender: UIBarButtonItem) {

        self.performSegue(withIdentifier: "ThemeSelectorSegue", sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ThemeSelectorSegue" {
            if let destinationViewController = (segue.destination as? UINavigationController) {
                if let themesViewController = destinationViewController.viewControllers.filter({$0 is ThemesSwiftViewController}).first as? ThemesSwiftViewController {
                   
                    themesViewController.choosenTheme = {  (theme: ColorTheme) in
                                                
                        ThemesManager.setThemeWithMainColor(theme)
                        ThemesManager.saveChoosenTheme(theme)
                    }
                }
            }
        }
    }
}

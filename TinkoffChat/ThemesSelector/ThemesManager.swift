//
//  ThemesManager.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/23/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

let storedThemeID: String = "STORED_THEME_ID"

class ThemesManager: NSObject {
    
    static func saveChoosenTheme(_ theme: ColorTheme) {
        
        UserDefaults.standard.set(theme.rawValue, forKey: storedThemeID)
        UserDefaults.standard.synchronize()
        
    }
    
    static func retriveStoredTheme() -> ColorTheme? {
        
        let storedValue = UserDefaults.standard.integer(forKey: storedThemeID)
        if storedValue != 0 {
            return ColorTheme(rawValue: storedValue)
        }
        return nil

    }
    
    static func setThemeWithMainColor(_ theme: ColorTheme) {

        var mainColor: UIColor?
        var tintColor: UIColor?
        let themes = Themes()
        switch theme {
        case .themeLight:
            
            mainColor = themes.themeLight
            tintColor = themes.themeDark
            break
        case .themeDark:
            mainColor = themes.themeDark
            tintColor = themes.themeLight
            break
        case .themeTinkoff:
            mainColor = themes.themeTinkoff
            tintColor = UIColor.black
            break
        }
        
        UINavigationBar.appearance().barTintColor = mainColor
        UINavigationBar.appearance().tintColor = tintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : tintColor!]
        
        let windows = UIApplication.shared.windows
        for window in windows {
            for view in window.subviews {
                view.removeFromSuperview()
                window.addSubview(view)
            }
        }

    }
}

//
//  Themes.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/29/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

enum ColorTheme: Int {
    case themeLight = 1
    case themeDark = 2
    case themeTinkoff = 3
}

class Themes {
    
    var themeLight: UIColor
    var themeDark: UIColor
    var themeTinkoff: UIColor
    
    init() {
        self.themeLight = UIColor(red: 0.878, green: 0.859, blue: 0.792, alpha: 1.0)
        self.themeDark = UIColor(red: 0.333, green: 0.325, blue: 0.298, alpha: 1.0)
        self.themeTinkoff = UIColor(red: 1.000, green: 0.865, blue: 0.175, alpha: 1.0)
        
    }
}


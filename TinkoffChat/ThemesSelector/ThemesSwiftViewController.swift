//
//  ThemesSwiftViewController.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/23/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

typealias ChoosenTheme = (_ theme: ColorTheme) -> Void

class ThemesSwiftViewController: UIViewController {

    var themes: Themes?
    var choosenTheme: ChoosenTheme?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        themes = Themes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func changeThemeAction(_ sender: UIButton) {
        choosenTheme?(ColorTheme(rawValue: sender.tag)!)
    }
}

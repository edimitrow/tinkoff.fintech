//
//  ConversationStorage.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import Foundation

extension Array {
    mutating func shuffle() {
        for i in 0 ..< (count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            swapAt(i, j)
        }
    }
}

let personNames = ["George", "John", "Thomas", "James", "Monroe", "Jackson", "Quincy", "Van Buren", "Harrison", "Tyler", "Polk", "Taylor", "Millard Fillmore", "Pierce", "Frank", "Wilson", "Taft", "McKinley", "Abe", "Donald", "Adams"]

class ConversationStorage {
    
    var chats: [[Person]]
    
    init() {
        self.chats = []
        var online: [Person] = []
        var offline: [Person] = []
        
        for name in personNames {
            let person = Person()
            person.name = name
            person.messages = self.mocMessages()
            person.isActive = false
            let isRead: Bool = Int(arc4random_uniform(2)) == 0 ? true: false
            if !isRead {
                person.messages?.last?.isRead = isRead
            }
            
            if online.count < 10 {
                person.isOnline = true
                online.append(person)
            } else {
                person.isOnline = false
                offline.append(person)
            }
        }
        
        let emptyPersonOnline = Person()
        emptyPersonOnline.name = "Eddie"
        emptyPersonOnline.isOnline = true
        emptyPersonOnline.messages = nil
        emptyPersonOnline.isActive = false
        online.append(emptyPersonOnline)
        
        let emptyPersonOffline = Person()
        emptyPersonOffline.name = "Buddy"
        emptyPersonOffline.isOnline = false
        emptyPersonOffline.messages = nil
        emptyPersonOffline.isActive = false
        offline.append(emptyPersonOffline)
        
        self.chats.append(online)
        self.chats.append(offline)
    }
    
    func mocMessages() -> [Message] {
        
        var messages: [Message] = []
        
        let messageOneIn = Message()
        messageOneIn.text = "!"
        messageOneIn.isRead = true
        messageOneIn.isIncoming = true
        messageOneIn.date = DateHelper.getRandomDate()
        
        let messageTwoIn = Message()
        messageTwoIn.text = "There was a person called Nana"
        messageTwoIn.isRead = true
        messageTwoIn.isIncoming = true
        messageTwoIn.date = DateHelper.getRandomDate()
        
        let messageThreeIn = Message()
        messageThreeIn.text = "There was once a velveteen rabbit, and in the beginning he was really splendid. He was fat and bunchy, as a rabbit should be; his coat was spotted brown and white, he had real thread whiskers, and his ears were lined with pink sateen. On Christmas morning, when he sat wedged in the top of the Boy's stocking, with a sprig of holly between his paws, the effect was charming."
        messageThreeIn.isRead = true
        messageThreeIn.isIncoming = true
        messageThreeIn.date = DateHelper.getRandomDate()
        
        let messageOneOut = Message()
        messageOneOut.text = ")"
        messageOneOut.isRead = true
        messageOneOut.isIncoming = false
        messageOneOut.date = DateHelper.getRandomDate()
        
        let messageTwoOut = Message()
        messageTwoOut.text = "The Boy sat up in bed and stretched out his hands"
        messageTwoOut.isRead = true
        messageTwoOut.isIncoming = false
        messageTwoOut.date = DateHelper.getRandomDate()
        
        let messageThreeOut = Message()
        messageThreeOut.text = "That night he was almost too happy to sleep, and so much love stirred in his little sawdust heart that it almost burst. And into his boot-button eyes, that had long ago lost their polish, there came a look of wisdom and beauty, so that even Nana noticed it next morning when she picked him up, and said, \"I declare if that old Bunny hasn't got quite a knowing expression!\""
        messageThreeOut.isRead = true
        messageThreeOut.isIncoming = false
        messageThreeOut.date = DateHelper.getRandomDate()
        
        messages.append(messageOneIn)
        messages.append(messageTwoIn)
        messages.append(messageThreeIn)
        messages.append(messageOneOut)
        messages.append(messageTwoOut)
        messages.append(messageThreeOut)
        
        messages.shuffle()
        return messages
    }
}


//
//  Person.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import Foundation

class Person {
    
    var name: String?
    var messages: [Message]?
    var isOnline: Bool?
    var isActive: Bool? 
}

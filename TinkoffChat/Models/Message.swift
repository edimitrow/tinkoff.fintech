//
//  Message.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 3/13/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import Foundation

class Message {
    
    var text: String?
    var date: Date?
    var isRead: Bool?
    var isIncoming: Bool?
}

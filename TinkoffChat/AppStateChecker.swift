//
//  AppStateChecker.swift
//  TinkoffChat
//
//  Created by Eugene Dimitrow on 2/24/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class AppStateChecker {

    var lastState: Int?
    
    var currentState: Int? {
        willSet(newState) {
            self.lastState = currentState
        }
    }
    
    init(_ state: Int) {
        self.currentState = state
        self.lastState = state
    }
    
    private func getStateValue(_ state: Int) -> String {
        
        switch state {
        case UIApplicationState.active.rawValue:
            return "Active"
        case UIApplicationState.inactive.rawValue:
            return "Inactive"
        case UIApplicationState.background.rawValue:
            return "Background"
        default:
            return "Unknown"
        }
    }
    
    func getStateFlow() -> String {
        
        let current = getStateValue(self.currentState!)
        var last = getStateValue(self.lastState!)
        
        if current == last {
            if last == getStateValue(UIApplicationState.inactive.rawValue) {
                last = "Not Running"
            } else {
                return String(format:" *** App is in inactive since it transitions to a different state")
            }
        }
        
        return String(format:" *** App goes to <\(current)> from <\(last)>")
    }
}
